package sari.ika.hallopasien

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_kamar.*


class  AdapterDataKamar(val dataProdi: List<HashMap<String,String>>,
                       val prodiActivity: KamarActivity) : //new
    RecyclerView.Adapter<AdapterDataKamar.HolderDataProdi>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataKamar.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kamar,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(p0: AdapterDataKamar.HolderDataProdi, p1: Int) {
        val data = dataProdi.get(p1)
        p0.txIdProdi.setText(data.get("id_kamar"))
        p0.txNamaProdi.setText(data.get("nama_kamar"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout2.setOnClickListener(View.OnClickListener {
            prodiActivity.edIdKam.setText(data.get("id_kamar"))
            prodiActivity.edNamaKam.setText(data.get("nama_kamar"))
        })
        //endNew
    }

    class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v){
        val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
        val txNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.cLayout2) //new
    }
}
