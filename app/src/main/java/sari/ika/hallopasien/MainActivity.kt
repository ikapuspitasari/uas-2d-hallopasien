package sari.ika.hallopasien

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPas.setOnClickListener(this)
        btnKam.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPas ->{
                var intent = Intent(this,PasienActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKam ->{
                var intent = Intent(this, KamarActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
